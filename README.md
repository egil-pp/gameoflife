# Game of Life+

# Table of Contents
1. [Introduction](#introduction)
2. [Rules of Conway's Game of Life](#rules-of-conways-game-of-life)
3. [Features](#features)
4. [DEMO](#demo)
5. [Starting and Stopping the Project](#starting-and-stopping-the-project)
6. [Technology Used](#technology-used)

## Introduction
Game of Life+ is a personal project I developed in early 2023. It's a web application that simulates Conway's Game of Life, but with a unique twist: cells are born with the average RGB values of their three parent cells, leading to unique and visually striking patterns.

## Rules of Conway's Game of Life
Conway's Game of Life is a cellular automaton devised by mathematician John Horton Conway. It's a zero-player game, meaning its evolution is determined by its initial state. The rules are as follows:
1. **Birth**: A dead cell with exactly three live neighbors becomes a live cell.
2. **Survival**: A live cell with two or three live neighbors stays alive.
3. **Death**: 
   - A live cell with fewer than two live neighbors dies (underpopulation).
   - A live cell with more than three live neighbors dies (overpopulation).

The game unfolds on a grid of cells where each cell is in one of two states: alive or dead. Each cell interacts with its eight neighbors (horizontal, vertical, and diagonal) to determine its next state.

## Features

### Feature #1: Colorful Offspring
- Offspring cells inherit RGB values that are the averages of three parent cells, creating a vivid and dynamic display.

### Feature #2: Speed Control
- Intuitive controls to adjust the speed of the game, allowing for flexible and varied gameplay experiences.

### Feature #3: Random Scene Generation
- A function to generate random, colorful scenes, adding an element of unpredictability and creativity.

### Feature #4: Dynamic Grid Rendering
- The grid dynamically adjusts to fit the screen size, ensuring a seamless experience across devices, including mobile.

### Feature #5: Service Worker Integration
- Incorporates a service worker in the React code, enabling the web app to function like a mobile application on various devices.

### Feature #6: Easy Deployment with Docker
- Quick and straightforward deployment using Docker compose, facilitating ease of use and accessibility.

## DEMO
Experience the game at [game.eglab.dev](https://game.eglab.dev).

## Starting and Stopping the Project

### Starting the Project
To start the project, use the following Docker Compose command. This will build and launch all the necessary containers in detached mode, allowing them to run in the background.

```docker compose up -d --build```

This command performs two main actions:

1. --build: Builds the images for the containers if they don't already exist or need updating.
2. -d: Runs the containers in detached mode, freeing up the terminal and running the containers in the background.

### Stopping the Project
When you're finished and want to stop all the running containers associated with the project, use the following command:

```docker compose down```

This command safely stops and removes all the containers, networks, and volumes created by docker compose up, cleaning up the system.

## Technology Used
- **React**: For building the user interface and managing the application state.
- **HTML, CSS, and JavaScript**: Employed for crafting the front-end and styling.
- **Docker**: Used for containerizing the application, simplifying deployment and scalability.

**Note**: Game of Life+ is an exploration into advanced web app development, combining classic algorithmic concepts with modern web technologies.
