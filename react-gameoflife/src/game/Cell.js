import React from 'react';
import './Game.css';

class Cell extends React.Component {
    render() {
        const { RGB, x, y, cell_size } = this.props;
        return (
            <div className="Cell" style={{
                left: `${cell_size * x + 1}px`,
                top: `${cell_size * y + 1}px`,
                width: `${cell_size - 1}px`,
                height: `${cell_size - 1}px`,
                background: `rgb(${RGB[0]}, ${RGB[1]}, ${RGB[2]})`
            }} />
        );
    }
}

export default Cell;