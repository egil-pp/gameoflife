import React, { useState, useEffect } from 'react';
import './SpeedDial.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';


const SpeedDial = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const [up, setUp] = useState(props.up || ['red', 'blue', 'green']);
  const [left, setLeft] = useState(props.left || ['clear', 'play', 'speed', 'random']);

  useEffect(() => {
    setLeft(props.left)
  }, [props.left]); // Pass propToWatch in the dependency array

  const handleButtonClick = (action) => {
    if (props.leftButtonActions && props.leftButtonActions[action]) {
      props.leftButtonActions[action]();
    }
  };

  const toggleSpeedDial = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className={`speed-dial-container ${isOpen ? 'open' : ''}`}>
      <button className="speed-dial-main" onClick={toggleSpeedDial}>
        <FontAwesomeIcon icon={faPlus} />
      </button>
      <div className="speed-dial-buttons">
      {left.map(({ label, icon }) => (
        <button
          key={label}
          className={`speed-dial-button ${label} speed-dial-util`}
          onClick={() => handleButtonClick(label)}
        >
          <FontAwesomeIcon icon={icon} />
        </button>
      ))}
      </div>
      <div className="speed-dial-colors">
        {up.map(({ label, action }) => (
          <button
            key={label}
            className={`speed-dial-button ${label} speed-dial-color`}
            onClick={action}
          >
            <div style={{backgroundColor: label}} className='color'></div>
          </button>
        ))}
      </div>
    </div>
  );
};

export default SpeedDial;
