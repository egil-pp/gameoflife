import React from 'react';
import Board from './Board';
import SpeedDial from './SpeedDial';
import './Game.css';
import { faTrash, faPause, faPlay, faImage, faRandom, faPersonWalkingWithCane, faPersonWalking, faPersonRunning, faTruckFast} from '@fortawesome/free-solid-svg-icons';


const CELL_SIZE = 20;
const WIDTH = Math.round((window.innerWidth-100) / 20) * 20;
const HEIGHT = Math.round((window.innerHeight-100) / 20) * 20;

class Game extends React.Component {
    constructor() {
        super();
        this.rows = HEIGHT / CELL_SIZE;
        this.cols = WIDTH / CELL_SIZE;
        this.mouseDown = false;
        this.board = this.makeEmptyBoard();
        this.R_G_B = [255, 0, 0]
        this.fileInputRef = React.createRef();

        this.state = {
            cells: [],
            isRunning: false,
            interval: 100,
            mouseX: null,
            mouseY: null,
            leftButtons: [
                { label: 'clear', icon: faTrash },
                { label: 'play', icon: faPlay },
                { label: 'speed', icon: faPersonWalking },
                { label: 'random', icon: faRandom },
                { label: 'experimental', icon: faImage },
            ],
            upButtons: [
                { label: 'red', action: () => this.changeColor([255,0,0]) },
                { label: 'green', action: () => this.changeColor([0,255,0]) },
                { label: 'blue', action: () => this.changeColor([0,0,255]) }
            ],
            imageArray: [],
            width: Math.round((window.innerWidth - 100) / 20) * 20,
            height: Math.round((window.innerHeight - 100) / 20) * 20
        }
    }

    speedIcons = {
        1: faTruckFast,
        10: faPersonRunning,
        100: faPersonWalking,
        1000: faPersonWalkingWithCane
    };

    leftButtonActions = {
        clear: () => this.handleClear(),
        play: () => this.toggleGame(),
        speed: () => this.handleIntervalChange(),
        random: () => this.handleRandom(),
        experimental: () => this.handleImage()
      };
    

    makeEmptyBoard() {
        let board = [];
        for (let y = 0; y < this.rows; y++) {
            board[y] = [];
            for (let x = 0; x < this.cols; x++) {
                board[y][x] = false;
            }
        }

        return board;
    }

    resizeBoard() {
        const newRows = this.board.length
        const newCols = newRows > 0 ? this.board[0].length : 0;

        // Create a new board with newRows rows
        const newBoard = new Array(this.rows).fill(null).map(() => new Array(this.cols).fill(false));
        

        // Copy values from the old board to the new one
        for (let i = 0; i < Math.min(this.rows, newRows); i++) {
            for (let j = 0; j < Math.min(this.cols, newCols); j++) {
                newBoard[i][j] = this.board[i][j];
            }
        }

        return newBoard;
    }

    getArrayShape(arr) {
        const numRows = arr.length;
        const numCols = numRows > 0 ? arr[0].length : 0;
        return [numRows, numCols];
    }

    getElementOffset() {
        const rect = this.boardRef.getBoundingClientRect();
        const doc = document.documentElement;

        return {
            x: (rect.left + window.pageXOffset) - doc.clientLeft,
            y: (rect.top + window.pageYOffset) - doc.clientTop,
        };
    }

    makeCells() {
        let cells = [];
        for (let y = 0; y < this.rows; y++) {
            for (let x = 0; x < this.cols; x++) {
                if (this.board[y][x]) {
                    cells.push({ x, y, RGB:this.board[y][x] });
                }
            }
        }

        return cells;
    }

    componentDidMount() {
        this.handleResize();  // Call this once to make sure the state is updated with the initial window size
        window.addEventListener('resize', this.handleResize);
      }
    
      componentWillUnmount() {
        // Clean up event listener when component is unmounted
        window.removeEventListener('resize', this.handleResize);
      }
    
      handleResize = () => {
        this.setState({
          width: Math.round((window.innerWidth - 100) / 20) * 20,
          height: Math.round((window.innerHeight - 100) / 20) * 20
        }, () => {
          this.rows = this.state.height / CELL_SIZE;
          this.cols = this.state.width / CELL_SIZE;
          this.board = this.resizeBoard();
          this.setState({ cells: this.makeCells() });
        });
      }

    handleClick = (event) => {

        const elemOffset = this.getElementOffset();
        const offsetX = event.clientX - elemOffset.x;
        const offsetY = event.clientY - elemOffset.y;
        
        const x = Math.floor(offsetX / CELL_SIZE);
        const y = Math.floor(offsetY / CELL_SIZE);

        if (x >= 0 && x <= this.cols && y >= 0 && y <= this.rows) {
            if(this.board[y][x]) {
                this.board[y][x] = false
            }
            else {
                this.board[y][x] = this.R_G_B
            }
        }

        this.setState({ cells: this.makeCells() });
    }

    handleMouseDown = () => {
        this.mouseDown = true
    }
    
    handleMouseUp = () => {
        this.mouseDown = false
    }

    changeColor = (color) => {
        this.R_G_B = color
    }

    handleDrag = (event) => {
        if (this.mouseDown) {
            const elemOffset = this.getElementOffset();
            const x = event.clientX - elemOffset.x;
            const y = event.clientY - elemOffset.y;

            const mouse_X = Math.floor(x / CELL_SIZE);
            const mouse_Y = Math.floor(y / CELL_SIZE);

            this.setState({ mouseX: mouse_X, mouseY: mouse_Y });

            if (mouse_X >= 0 && mouse_X < this.cols && mouse_Y >= 0 && mouse_Y < this.rows) {
                this.board[mouse_Y][mouse_X] = this.R_G_B;
            }
    
            this.setState({ cells: this.makeCells() });
        }
    }

    handleTouchDrag = (event) => {
        if (this.mouseDown) {
            const elemOffset = this.getElementOffset();
            const x = event.touches[0].clientX - elemOffset.x;
            const y = event.touches[0].clientY - elemOffset.y;

            const mouse_X = Math.floor(x / CELL_SIZE);
            const mouse_Y = Math.floor(y / CELL_SIZE);

            this.setState({ mouseX: mouse_X, mouseY: mouse_Y });

            if (mouse_X >= 0 && mouse_X < this.cols && mouse_Y >= 0 && mouse_Y < this.rows) {
                this.board[mouse_Y][mouse_X] = this.R_G_B;
            }
    
            this.setState({ cells: this.makeCells() });
        }
    }

    toggleGame = () => {
        if (this.state.isRunning) {
          // Stop the game
          this.setState({ isRunning: false });
        
          const newButtons = this.state.leftButtons.map(button => {
            if (button.label === 'play') {
              return { ...button, icon: faPlay }; // return a new object with the new icon
            }
            return button; // return the original object if it's not the 'clear' button
          });

          this.setState({ leftButtons: newButtons })

          if (this.timeoutHandler) {
            window.clearTimeout(this.timeoutHandler);
            this.timeoutHandler = null;
          }
        } else {
          // Start the game
          this.setState({ isRunning: true });

          const newButtons = this.state.leftButtons.map(button => {
            if (button.label === 'play') {
              return { ...button, icon: faPause }; // return a new object with the new icon
            }
            return button; // return the original object if it's not the 'clear' button
          });

          this.setState({ leftButtons: newButtons })

          this.runIteration();
        }
    };

    runIteration() {
        let newBoard = this.makeEmptyBoard();

        for (let y = 0; y < this.rows; y++) {
            for (let x = 0; x < this.cols; x++) {
                let result = this.calculateNeighbors(this.board, x, y);
                let neighbors = result.neighbors
                let avgColor = result.avgColor
                if (this.board[y][x]) {
                    if (neighbors === 2 || neighbors === 3) {
                        newBoard[y][x] = avgColor;
                    } else {
                        newBoard[y][x] = false;
                    }
                } else {
                    if (!this.board[y][x] && neighbors === 3) {
                        newBoard[y][x] = avgColor;
                    }
                }
            }
        }

        this.board = newBoard;
        this.setState({ cells: this.makeCells() });

        this.timeoutHandler = window.setTimeout(() => {
            this.runIteration();
        }, this.state.interval);
    }

    calculateAverage(arr) {
        let sumX = 0;
        let sumY = 0;
        let sumZ = 0;
        let n = arr.length;
    
        for (let i = 0; i < n; i++) {
            sumX += arr[i][0];
            sumY += arr[i][1];
            sumZ += arr[i][2];
        }
    
        let avgX = sumX / n;
        let avgY = sumY / n;
        let avgZ = sumZ / n;
    
        return [avgX, avgY, avgZ];
    }

    calculateNeighbors(board, x, y) {
        let neighbors = 0;
        let colors = [];
        const dirs = [[-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1]];
        for (let i = 0; i < dirs.length; i++) {
            const dir = dirs[i];
            let y1 = y + dir[0];
            let x1 = x + dir[1];
    
            if (x1 >= 0 && x1 < this.cols && y1 >= 0 && y1 < this.rows && board[y1][x1]) {
                neighbors++;
                colors.push(board[y1][x1]);
            }
        }
        
        let avgColor = this.calculateAverage(colors)

        return { neighbors: neighbors, avgColor: avgColor }
    }
    

    handleIntervalChange = () => {
        const nextNumber = this.state.interval < 1000 ? this.state.interval * 10 : 1;
        this.setState({ interval: nextNumber });

        const newButtons = this.state.leftButtons.map(button => {
            if (button.label === 'speed') {
              return { ...button, icon: this.speedIcons[nextNumber] }; // return a new object with the new icon
            }
            return button; // return the original object if it's not the 'clear' button
        });

        this.setState({ leftButtons: newButtons })
    }

    handleClear = () => {
        this.board = this.makeEmptyBoard();
        this.setState({ cells: this.makeCells() });
    }

    handleRandom = () => {
        for (let y = 0; y < this.rows; y++) {
            for (let x = 0; x < this.cols; x++) {
                let value = (Math.random() >= 0.5);
                if(value) {
                    this.board[y][x] = this.getRandomElement([[255,0,0],[0,255,0],[0,0,255]])
                }
            }
        }

        this.setState({ cells: this.makeCells() });
    }

    getRandomElement(arr) {
        if (arr.length === 0) {
            return null;
        }
        
        const randomIndex = Math.floor(Math.random() * arr.length);
        return arr[randomIndex];
    }

    handleImage = () => {
        // Programmatically click the hidden file input
        this.fileInputRef.current.click();
    };

    handleImageUpload = (event) => {
        const file = event.target.files[0];
        const img = new Image();
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');
    
        img.onload = () => {
          const targetWidth = this.cols;
          const targetHeight = this.rows;
          canvas.width = targetWidth;
          canvas.height = targetHeight;
          ctx.drawImage(img, 0, 0, targetWidth, targetHeight);
    
          const imageData = ctx.getImageData(0, 0, targetWidth, targetHeight);
          const data = imageData.data;
          let arr = [];
    
          for (let y = 0; y < targetHeight; y++) {
            let row = [];
            for (let x = 0; x < targetWidth; x++) {
              const index = (y * targetWidth + x) * 4;
              if(!this.ColorDistance(data[index], data[index+1], data[index+2])) {
                this.board[y][x] = [data[index], data[index+1], data[index+2]]
                row.push({
                    r: data[index],
                    g: data[index + 1],
                    b: data[index + 2]
                });
              }
            }
            arr.push(row);
          }
    
          this.setState({ imageArray: arr });
          console.log("Image array:", arr); // Log the array to the console
          console.log(arr.length)
          console.log(arr[0].length)

          this.setState({ cells: this.makeCells() });
        };
    
        if (file) {
          img.src = URL.createObjectURL(file);
        }
      };
    
    ColorDistance(r, g, b, threshold = 100) {
        // Calculate the distance from black
        const distanceFromBlack = Math.sqrt(r * r + g * g + b * b);
    
        // Calculate the distance from white
        const distanceFromWhite = Math.sqrt((255 - r) * (255 - r) + (255 - g) * (255 - g) + (255 - b) * (255 - b));
    
        // Check if the color is close to either white or black
        return distanceFromBlack <= threshold || distanceFromWhite <= threshold;
    }
    

    render() {
        const { cells, interval, isRunning, mouseX, mouseY, width, height } = this.state;
        return (
          <div>
            <Board
              cells={cells}
              onMouseDown={this.handleMouseDown}
              onMouseUp={this.handleMouseUp}
              onMouseMove={this.handleDrag}
              onTouchMove={this.handleTouchDrag}
              onClick={this.handleClick}
              boardRef={(n) => { this.boardRef = n; }}
              width={width}
              height={height}
              cell_size={CELL_SIZE}
            />
            <SpeedDial
              left={this.state.leftButtons}
              up={this.state.upButtons}
              leftButtonActions={this.leftButtonActions}
            />
            <input 
                type="file"
                accept="image/*"
                style={{ display: 'none' }} 
                ref={this.fileInputRef} 
                onChange={this.handleImageUpload} 
            />
          </div>
        );
    }
}

export default Game;
