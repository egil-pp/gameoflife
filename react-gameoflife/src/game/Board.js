import React from 'react';
import Cell from './Cell';
import './Game.css';

class Board extends React.Component {
    render() {
        const { cells, onMouseDown, onMouseUp, onMouseMove, onTouchMove, onClick, boardRef, width, height, cell_size } = this.props;
        return (
            <div className="Board"
                style={{ width: width, height: height, backgroundSize: `${cell_size}px ${cell_size}px`}}
                onMouseDown={onMouseDown}
                onMouseUp={onMouseUp}
                onMouseMove={onMouseMove}
                onClick={onClick}
                onTouchStart={onMouseDown}
                onTouchEnd={onMouseUp}
                onTouchMove={onTouchMove}
                ref={boardRef}>
                {cells.map(cell => (
                    <Cell x={cell.x} y={cell.y} RGB={cell.RGB} cell_size={cell_size} key={`${cell.x},${cell.y}`}/>
                ))}
            </div>
        );
    }
}

export default Board;
